#ifndef BMI270_H
#define BMI270_H

#include <stdint.h>
#include "user_define.h"
#include "bmi2_defs.h"


/*! Macro that defines read write length */
#define READ_WRITE_LEN     UINT8_C(32)


void bmi2_error_codes_print_result(int8_t rslt);
int8_t Open_BMI270_ACC(struct bmi2_dev *dev);
int8_t Close_BMI270_ACC(struct bmi2_dev *dev);
int8_t Open_BMI270_GYRO(struct bmi2_dev *dev);
int8_t Close_BMI270_GYRO(struct bmi2_dev *dev);
int8_t Open_BMI270_WRISTWEAR_WAKEUP(struct bmi2_dev *dev);
int8_t Close_BMI270_WRISTWEAR_WAKEUP(struct bmi2_dev *dev);
#if defined(FIFO_POLL) || defined(FIFO_WM_INT)
int8_t Open_BMI270_FIFO(struct bmi2_dev *dev);
#endif
int8_t Close_BMI270_FIFO(struct bmi2_dev *dev);
void BMI270_Print_ALLRegs(struct bmi2_dev *dev);
void Init_bmi270();
int8_t Init_BMI270(struct bmi2_dev *dev);
void StartBMI270Task();
void read_Gryo(uint16_t *res);
void read_Accel(uint16_t *res);
#if defined(FIFO_POLL) || defined(FIFO_WM_INT) || defined(STEP_COUNTER)
//void BMI270_Timer_Callback(TimerHandle_t xTimer);
#endif
#if defined(STEP_COUNTER)
int8_t Modify_BMI270_STEP_COUNTER_Parameter(struct bmi2_dev *dev);
int8_t Open_BMI270_STEP_COUNTER(struct bmi2_dev *dev);
int8_t Close_BMI270_STEP_COUNTER(struct bmi2_dev *dev);
#endif

#if defined(WRIST_WEAR_WAKE_UP)
void StartBMI270InterruptTask(void const * argument);
#endif


#endif
