#include <stdio.h>
#include <string.h>
#include "common_porting.h"
//#include "cmsis_os.h"
#include "stm32g4xx_hal.h"
#include "bmi270_task.h"
#include "bmi270.h"
#include "i2c.h"

extern volatile uint8_t int1_flag;
extern volatile uint8_t int2_flag;

struct bmi2_dev bmi270dev;
uint8_t bmi270_dev_addr;

#if defined(ACC_ONLY) || defined(GYRO_ONLY) || defined(ACC_GYRO)
struct bmi2_sensor_data sensor_data = { 0 };
#if defined(STEP_COUNTER)
struct bmi2_sensor_data sensor_data_stepcounter = { 0 };
#endif
#elif defined(WRIST_WEAR_WAKE_UP)
struct bmi2_sensor_data sensor_data_AG[4] = { 0 };
#elif defined(ACC_GYRO)
struct bmi2_sensor_data sensor_data_AG[3] = { 0 };
#endif
volatile uint16_t fifo_read_ready = 0;
#if defined(FIFO_POLL) || defined(FIFO_WM_INT) || defined(STEP_COUNTER)
//TimerHandle_t BMI270_Timer_Handler;
/* Number of accelerometer frames */
uint16_t accel_length;
/* Number of gyroscope frames */
uint16_t gyr_length;
/* Variable to index bytes */
uint16_t idx = 0;
/* Number of bytes of FIFO data */
uint8_t fifo_data[2048] = {0};
/* Array of accelerometer frames -> Total bytes =
150 * (6 axes bytes + 1 header byte) = 1050 bytes*/
struct bmi2_sens_axes_data fifo_accel_data[400] = { {0} };
struct bmi2_sens_axes_data fifo_gyr_data[400] = { {0} };
/* Initialize FIFO frame structure */
struct bmi2_fifo_frame fifoframe = {0};
#endif

/*!
 *  @brief Prints the execution status of the APIs.
 */
void bmi2_error_codes_print_result(int8_t rslt)
{
    switch (rslt)
    {
        case BMI2_OK:

            /* Do nothing */
            break;

        case BMI2_W_FIFO_EMPTY:
            PDEBUG("Warning [%d] : FIFO empty\r\n", rslt);
            break;
        case BMI2_W_PARTIAL_READ:
            PDEBUG("Warning [%d] : FIFO partial read\r\n", rslt);
            break;
        case BMI2_E_NULL_PTR:
            PDEBUG(
                "Error [%d] : Null pointer error. It occurs when the user tries to assign value (not address) to a pointer," " which has been initialized to NULL.\r\n",
                rslt);
            break;

        case BMI2_E_COM_FAIL:
            PDEBUG(
                "Error [%d] : Communication failure error. It occurs due to read/write operation failure and also due " "to power failure during communication\r\n",
                rslt);
            break;

        case BMI2_E_DEV_NOT_FOUND:
            PDEBUG("Error [%d] : Device not found error. It occurs when the device chip id is incorrectly read\r\n",
                   rslt);
            break;

        case BMI2_E_INVALID_SENSOR:
            PDEBUG(
                "Error [%d] : Invalid sensor error. It occurs when there is a mismatch in the requested feature with the " "available one\r\n",
                rslt);
            break;

        case BMI2_E_SELF_TEST_FAIL:
            PDEBUG(
                "Error [%d] : Self-test failed error. It occurs when the validation of accel self-test data is " "not satisfied\r\n",
                rslt);
            break;

        case BMI2_E_INVALID_INT_PIN:
            PDEBUG(
                "Error [%d] : Invalid interrupt pin error. It occurs when the user tries to configure interrupt pins " "apart from INT1 and INT2\r\n",
                rslt);
            break;

        case BMI2_E_OUT_OF_RANGE:
            PDEBUG(
                "Error [%d] : Out of range error. It occurs when the data exceeds from filtered or unfiltered data from " "fifo and also when the range exceeds the maximum range for accel and gyro while performing FOC\r\n",
                rslt);
            break;

        case BMI2_E_ACC_INVALID_CFG:
            PDEBUG(
                "Error [%d] : Invalid Accel configuration error. It occurs when there is an error in accel configuration" " register which could be one among range, BW or filter performance in reg address 0x40\r\n",
                rslt);
            break;

        case BMI2_E_GYRO_INVALID_CFG:
            PDEBUG(
                "Error [%d] : Invalid Gyro configuration error. It occurs when there is a error in gyro configuration" "register which could be one among range, BW or filter performance in reg address 0x42\r\n",
                rslt);
            break;

        case BMI2_E_ACC_GYR_INVALID_CFG:
            PDEBUG(
                "Error [%d] : Invalid Accel-Gyro configuration error. It occurs when there is a error in accel and gyro" " configuration registers which could be one among range, BW or filter performance in reg address 0x40 " "and 0x42\r\n",
                rslt);
            break;

        case BMI2_E_CONFIG_LOAD:
            PDEBUG(
                "Error [%d] : Configuration load error. It occurs when failure observed while loading the configuration " "into the sensor\r\n",
                rslt);
            break;

        case BMI2_E_INVALID_PAGE:
            PDEBUG(
                "Error [%d] : Invalid page error. It occurs due to failure in writing the correct feature configuration " "from selected page\r\n",
                rslt);
            break;

        case BMI2_E_SET_APS_FAIL:
            PDEBUG(
                "Error [%d] : APS failure error. It occurs due to failure in write of advance power mode configuration " "register\r\n",
                rslt);
            break;

        case BMI2_E_AUX_INVALID_CFG:
            PDEBUG(
                "Error [%d] : Invalid AUX configuration error. It occurs when the auxiliary interface settings are not " "enabled properly\r\n",
                rslt);
            break;

        case BMI2_E_AUX_BUSY:
            PDEBUG(
                "Error [%d] : AUX busy error. It occurs when the auxiliary interface buses are engaged while configuring" " the AUX\r\n",
                rslt);
            break;

        case BMI2_E_REMAP_ERROR:
            PDEBUG(
                "Error [%d] : Remap error. It occurs due to failure in assigning the remap axes data for all the axes " "after change in axis position\r\n",
                rslt);
            break;

        case BMI2_E_GYR_USER_GAIN_UPD_FAIL:
            PDEBUG(
                "Error [%d] : Gyro user gain update fail error. It occurs when the reading of user gain update status " "fails\r\n",
                rslt);
            break;

        case BMI2_E_SELF_TEST_NOT_DONE:
            PDEBUG(
                "Error [%d] : Self-test not done error. It occurs when the self-test process is ongoing or not " "completed\r\n",
                rslt);
            break;

        case BMI2_E_INVALID_INPUT:
            PDEBUG("Error [%d] : Invalid input error. It occurs when the sensor input validity fails\r\n", rslt);
            break;

        case BMI2_E_INVALID_STATUS:
            PDEBUG("Error [%d] : Invalid status error. It occurs when the feature/sensor validity fails\r\n", rslt);
            break;

        case BMI2_E_CRT_ERROR:
            PDEBUG("Error [%d] : CRT error. It occurs when the CRT test has failed\r\n", rslt);
            break;

        case BMI2_E_ST_ALREADY_RUNNING:
            PDEBUG(
                "Error [%d] : Self-test already running error. It occurs when the self-test is already running and " "another has been initiated\r\n",
                rslt);
            break;

        case BMI2_E_CRT_READY_FOR_DL_FAIL_ABORT:
            PDEBUG(
                "Error [%d] : CRT ready for download fail abort error. It occurs when download in CRT fails due to wrong " "address location\r\n",
                rslt);
            break;

        case BMI2_E_DL_ERROR:
            printf(
                "Error [%d] : Download error. It occurs when write length exceeds that of the maximum burst length\r\n",
                rslt);
            break;

        case BMI2_E_PRECON_ERROR:
            PDEBUG(
                "Error [%d] : Pre-conditional error. It occurs when precondition to start the feature was not " "completed\r\n",
                rslt);
            break;

        case BMI2_E_ABORT_ERROR:
            PDEBUG("Error [%d] : Abort error. It occurs when the device was shaken during CRT test\r\n", rslt);
            break;

        case BMI2_E_WRITE_CYCLE_ONGOING:
            PDEBUG(
                "Error [%d] : Write cycle ongoing error. It occurs when the write cycle is already running and another " "has been initiated\r\n",
                rslt);
            break;

        case BMI2_E_ST_NOT_RUNING:
            PDEBUG(
                "Error [%d] : Self-test is not running error. It occurs when self-test running is disabled while it's " "running\r\n",
                rslt);
            break;

        case BMI2_E_DATA_RDY_INT_FAILED:
            PDEBUG(
                "Error [%d] : Data ready interrupt error. It occurs when the sample count exceeds the FOC sample limit " "and data ready status is not updated\r\n",
                rslt);
            break;

        case BMI2_E_INVALID_FOC_POSITION:
            PDEBUG(
                "Error [%d] : Invalid FOC position error. It occurs when average FOC data is obtained for the wrong" " axes\r\n",
                rslt);
            break;

        default:
            PDEBUG("Error [%d] : Unknown error code\r\n", rslt);
            break;
    }
}

/*!
 *  @brief Function to select the interface between SPI and I2C.
 *  Also to initialize coines platform
 */
int8_t bmi2_interface_init(struct bmi2_dev *bmi, uint8_t intf)
{
	int8_t rslt = BMI2_OK;
	
	/* Bus configuration : I2C */
	if (intf == BMI2_I2C_INTF)
	{
		PDEBUG("I2C Interface \n");

		/* To initialize the user I2C function */
		bmi270_dev_addr = BMI2_I2C_PRIM_ADDR;
		bmi->intf = BMI2_I2C_INTF;
		bmi->read = (bmi2_read_fptr_t)SensorAPI_I2Cx_Read;
		bmi->write = (bmi2_write_fptr_t)SensorAPI_I2Cx_Write;
	}
	/* Bus configuration : SPI */
	else if (intf == BMI2_SPI_INTF)
	{
		PDEBUG("SPI Interface \n");
		bmi->intf = BMI2_SPI_INTF;
        PDEBUG("here is not exist SPI interface\n");
//		bmi->read = (bmi2_read_fptr_t)SensorAPI_SPIx_Read;
//		bmi->write = (bmi2_write_fptr_t)SensorAPI_SPIx_Write;
	}


	/* Assign device address to interface pointer */
	bmi->intf_ptr = &bmi270_dev_addr;

	/* Configure delay in microseconds */
	bmi->delay_us = bmi2_delay_us;

	/* Configure max read/write length (in bytes) ( Supported length depends on target machine) */
	bmi->read_write_len = READ_WRITE_LEN;

	/* Assign to NULL to load the default config file. */
	bmi->config_file_ptr = NULL;

	return rslt;
}

int8_t Open_BMI270_ACC(struct bmi2_dev *dev)
{
	int8_t rslt = BMI2_OK;
	uint8_t sensor_list = BMI2_ACCEL;
	/* Sensor configuration structure */
	struct bmi2_sens_config config = { 0 };

	/* Enable the selected sensors */
	rslt = bmi270_sensor_enable(&sensor_list, 1, dev);
	PDEBUG("bmi2_sensor_enable BMI2_ACCEL\r\n");
	bmi2_error_codes_print_result(rslt);

	config.type = sensor_list;
	
	/* Get the previous or default configuration settings */
	rslt = bmi270_get_sensor_config(&config, 1, dev);
	if (rslt == BMI2_OK) 
	{
		/* Update all or any of the accelerometer
		configurations */
		#if defined(SUPPORT_LOWPOWER)
		config.cfg.acc.filter_perf = BMI2_PERF_OPT_MODE;
		config.cfg.acc.bwp        = BMI2_ACC_NORMAL_AVG4;
		#else
		config.cfg.acc.filter_perf = BMI2_PERF_OPT_MODE;
		config.cfg.acc.bwp        = BMI2_ACC_NORMAL_AVG4;
		#endif
		
		config.cfg.acc.odr      = BMI2_ACC_ODR_100HZ;
		config.cfg.acc.range     = BMI2_ACC_RANGE_2G;
		
		/* Set the configurations */
		rslt = bmi270_set_sensor_config(&config, 1, dev);
		if (rslt != BMI2_OK) 
		{
			PDEBUG("ACC configuration failed, rslt=%d\r\n", rslt);
		} 
		else
		{
			PDEBUG("ACC configuration set successfully\r\n");

			/* Get the configuration settings for validation */
			rslt = bmi270_get_sensor_config(&config, 1, dev);
			if (rslt == BMI2_OK) 
			{
				PDEBUG("Get BMI2_ACCEL Configuration successful\r\n");
				PDEBUG("Performance Mode = %d\r\n", config.cfg.acc.filter_perf);
				PDEBUG("Bandwidth = %d\r\n", config.cfg.acc.bwp);
				PDEBUG("ODR = %d\r\n", config.cfg.acc.odr);
				PDEBUG("Range = %d\r\n", config.cfg.acc.range);
			}
		}
	}

	return rslt;
}

int8_t Close_BMI270_ACC(struct bmi2_dev *dev)
{
	int8_t rslt = BMI2_OK;
	uint8_t sensor_list = BMI2_ACCEL;

	/* Disable the selected sensors */
	rslt = bmi270_sensor_disable(&sensor_list, 1, dev);
	bmi2_error_codes_print_result(rslt);

	return rslt;
}

int8_t Open_BMI270_GYRO(struct bmi2_dev *dev)
{
	int8_t rslt = BMI2_OK;
	uint8_t sensor_list = BMI2_GYRO;
	/* Sensor configuration structure */
	struct bmi2_sens_config config = { 0 };

	/* Enable the selected sensors */
	rslt = bmi270_sensor_enable(&sensor_list, 1, dev);
	bmi2_error_codes_print_result(rslt);

	config.type = sensor_list;
	
	/* Get the previous or default configuration settings */
	rslt = bmi270_get_sensor_config(&config, 1, dev);
	if (rslt == BMI2_OK) 
	{
		config.cfg.gyr.odr = BMI2_GYR_ODR_100HZ;
		config.cfg.gyr.range = BMI2_GYR_RANGE_2000;
		config.cfg.gyr.ois_range = BMI2_GYR_OIS_2000;
		
		#if defined(SUPPORT_LOWPOWER)
		config.cfg.gyr.bwp = BMI2_GYR_OSR4_MODE;
		config.cfg.gyr.noise_perf = BMI2_POWER_OPT_MODE;
		config.cfg.gyr.filter_perf = BMI2_POWER_OPT_MODE;
		#else
		config.cfg.gyr.bwp = BMI2_GYR_NORMAL_MODE;
		config.cfg.gyr.noise_perf = BMI2_PERF_OPT_MODE;
		config.cfg.gyr.filter_perf = BMI2_PERF_OPT_MODE;
		#endif
	
		/* Set the configurations */
		rslt = bmi270_set_sensor_config(&config, 1, dev);
		if (rslt != BMI2_OK) 
		{
			PDEBUG("GYRO configuration failed\r\n");
		} 
		else
		{
			PDEBUG("GYRO configuration set successfully\r\n");

			/* Get the configuration settings for validation */
			rslt = bmi270_get_sensor_config(&config, 1, dev);
			if (rslt == BMI2_OK) 
			{
				PDEBUG("Get BMI2_GYRO Configuration successful\r\n");
				PDEBUG("Bandwidth = %d\r\n", config.cfg.gyr.bwp);
				PDEBUG("ODR = %d\r\n", config.cfg.gyr.odr);
				PDEBUG("Range = %d\r\n", config.cfg.gyr.range);
				PDEBUG("OIS Range = %d\r\n", config.cfg.gyr.ois_range);
				PDEBUG("Noise_perf = %d\r\n", config.cfg.gyr.noise_perf);
				PDEBUG("Filter_perf = %d\r\n", config.cfg.gyr.filter_perf);
			}
		}
	}

	return rslt;
}

int8_t Close_BMI270_GYRO(struct bmi2_dev *dev)
{
	int8_t rslt = BMI2_OK;
	uint8_t sensor_list = BMI2_GYRO;

	/* Disable the selected sensors */
	rslt = bmi270_sensor_disable(&sensor_list, 1, dev);
	bmi2_error_codes_print_result(rslt);

	return rslt;
}

int8_t Open_BMI270_WRISTWEAR_WAKEUP(struct bmi2_dev *dev)
{
	int8_t rslt = BMI2_OK;
	uint8_t sensor_list = BMI2_WRIST_WEAR_WAKE_UP;
	/* Sensor configuration structure */
	struct bmi2_sens_config config = { 0 };

	struct bmi2_int_pin_config int_cfg;
	
	/* Select features and their pins to be mapped to */
	struct bmi2_sens_int_config sens_int;
	sens_int.type = BMI2_WRIST_WEAR_WAKE_UP;
	sens_int.hw_int_pin = BMI2_INT1;

	/* Enable the selected sensors */
	rslt = bmi270_sensor_enable(&sensor_list, 1, dev);
	bmi2_error_codes_print_result(rslt);

	config.type = sensor_list;
	#if 1
	/* Get the previous or default configuration settings */
	rslt = bmi270_get_sensor_config(&config, 1, dev);
	if (rslt == BMI2_OK) 
	{
		#if 0
		config.cfg.wrist_wear_wake_up.min_angle_focus = 1774;//1677, 35�; 1774, 30�
		config.cfg.wrist_wear_wake_up_wh.min_angle_nonfocus = 1856;
		config.cfg.wrist_wear_wake_up_wh.angle_lr = 128;
		config.cfg.wrist_wear_wake_up_wh.angle_ll = 128;
		config.cfg.wrist_wear_wake_up_wh.angle_pd = 22;
		config.cfg.wrist_wear_wake_up_wh.angle_pu = 247;
		config.cfg.wrist_wear_wake_up_wh.min_dur_mov = 2;
		config.cfg.wrist_wear_wake_up_wh.min_dur_quite = 2;
		#endif
		
		/* Set the configurations */
		rslt = bmi270_set_sensor_config(&config, 1, dev);
		if (rslt != BMI2_OK) 
		{
			PDEBUG("WWW configuration failed\r\n");
		} 
		else
		{
			PDEBUG("WWW configuration set successfully\r\n");

			/* Get the configuration settings for validation */
			rslt = bmi270_get_sensor_config(&config, 1, dev);
			if (rslt == BMI2_OK) 
			{
				PDEBUG("Get BMI2_WRIST_WEAR_WAKE_UP Configuration successful\r\n");
			}
		}
	}
	#endif
	
	bmi2_get_int_pin_config(&int_cfg, dev);

	int_cfg.pin_type = BMI2_INT1;
	int_cfg.pin_cfg[0].lvl = BMI2_INT_ACTIVE_HIGH;/*Config INT1 rising edge trigging*/
	int_cfg.pin_cfg[0].od = BMI2_INT_PUSH_PULL;
	int_cfg.pin_cfg[0].output_en= BMI2_INT_OUTPUT_ENABLE;

	bmi2_set_int_pin_config(&int_cfg, dev);
	bmi2_error_codes_print_result(rslt);
	
	/* Map the feature interrupt */
	rslt = bmi270_map_feat_int(&sens_int, 1, dev);
	bmi2_error_codes_print_result(rslt);
	PDEBUG("Lift the board in portrait landscape position and tilt in a particular direction\r\n");

	return rslt;
}

int8_t Close_BMI270_WRISTWEAR_WAKEUP(struct bmi2_dev *dev)
{
	int8_t rslt = BMI2_OK;
	uint8_t sensor_list = BMI2_WRIST_WEAR_WAKE_UP_WH;
	struct bmi2_int_pin_config int_cfg;
	
	/* Select features and their pins to be mapped to */
	struct bmi2_sens_int_config sens_int;
	sens_int.type = BMI2_WRIST_WEAR_WAKE_UP;
	sens_int.hw_int_pin = BMI2_INT_NONE;

	/* Enable the selected sensors */
	rslt = bmi270_sensor_disable(&sensor_list, 1, dev);
	bmi2_error_codes_print_result(rslt);

	bmi2_get_int_pin_config(&int_cfg, dev);

	/*Set to default value*/
	int_cfg.pin_type = BMI2_INT_NONE;
	int_cfg.pin_cfg[0].lvl = BMI2_INT_ACTIVE_LOW;
	int_cfg.pin_cfg[0].od = BMI2_INT_PUSH_PULL;
	int_cfg.pin_cfg[0].output_en= BMI2_INT_OUTPUT_DISABLE;
	int_cfg.pin_cfg[0].input_en= BMI2_INT_INPUT_DISABLE;

	bmi2_set_int_pin_config(&int_cfg, dev);
	
	/* Map the feature interrupt */
	rslt = bmi270_map_feat_int(&sens_int, 1, dev);
	PDEBUG("bmi2_map_feat_int\r\n");
	bmi2_error_codes_print_result(rslt);
	PDEBUG("Lift the board in portrait landscape position and tilt in a particular direction\r\n");

	return rslt;
}

#if defined(FIFO_POLL) || defined(FIFO_WM_INT)
int8_t Open_BMI270_FIFO(struct bmi2_dev *dev)
{
	int8_t rslt = BMI2_OK;

	#if defined(FIFO_WM_INT)
	struct bmi2_int_pin_config int_cfg;
	uint8_t sens_int = BMI2_FWM_INT;
	#endif

	/* Disable advanced power save */
	rslt = bmi2_set_adv_power_save(BMI2_DISABLE, dev);
	if (rslt != BMI2_OK) 
	{
		PDEBUG("Error code: %d\r\n", rslt);
		return rslt;
	}

	/* Flush FIFO */
	rslt = bmi2_set_command_register(BMI2_FIFO_FLUSH_CMD, dev);
	if (rslt != BMI2_OK) 
	{
	        PDEBUG("Flush FIFO error, error code: %d\r\n", rslt);
	        return rslt;
	}

	/* Clear FIFO configuration register */
	rslt = bmi2_set_fifo_config(BMI2_FIFO_ALL_EN, BMI2_DISABLE, dev);
	if (rslt != BMI2_OK) 
	{
		PDEBUG("Clear FIFO configuration register error, error code: %d\r\n", rslt);
		return rslt;
	}

	/*Example: 100Hz ODR, read data per second*/
#if 0//FIFO with header mode
	
	PDEBUG("FIFO Header is enabled\r\n");
	#if defined(FIFO_POLL) || defined(FIFO_WM_INT)
		#if defined(ACC_ONLY)
		rslt = bmi2_set_fifo_wm(700, dev);//7*100=700 byte
		if (rslt != BMI2_OK) 
		{
			PDEBUG("bmi2_set_fifo_wm error, error code: %d\r\n", rslt);
		}
		rslt = bmi2_set_fifo_config(BMI2_FIFO_ACC_EN | BMI2_FIFO_HEADER_EN , BMI2_ENABLE, dev);
		#elif defined(GYRO_ONLY)
		rslt = bmi2_set_fifo_wm(700, dev);//7*100=700 byte
		if (rslt != BMI2_OK) 
		{
			PDEBUG("bmi2_set_fifo_wm error, error code: %d\r\n", rslt);
		}
		rslt = bmi2_set_fifo_config(BMI2_FIFO_GYR_EN | BMI2_FIFO_HEADER_EN , BMI2_ENABLE, dev);
		#elif defined(ACC_GYRO)
		rslt = bmi2_set_fifo_wm(1300, dev);//13*100=1300 byte
		if (rslt != BMI2_OK) 
		{
			PDEBUG("bmi2_set_fifo_wm error, error code: %d\r\n", rslt);
		}
		rslt = bmi2_set_fifo_config(BMI2_FIFO_ACC_EN | BMI2_FIFO_GYR_EN | BMI2_FIFO_HEADER_EN , BMI2_ENABLE, dev);
		#endif
		if (rslt != BMI2_OK)
		{
			PDEBUG("Set fifo config failed\r\n");
		}
	#endif
#else//enable acc and gyro without header
	/* Set FIFO configuration by enabling accelerometer and gyroscope*/
	PDEBUG("FIFO Header is disabled\r\n");

	#if defined(FIFO_POLL) || defined(FIFO_WM_INT)
		#if defined(ACC_ONLY)
		rslt = bmi2_set_fifo_wm(600, dev);//6*100=600
		if (rslt != BMI2_OK) 
		{
			PDEBUG("bmi2_set_fifo_wm error, error code: %d\r\n", rslt);
		}
		rslt = bmi2_set_fifo_config(BMI2_FIFO_ACC_EN , BMI2_ENABLE, dev);
		#elif defined(GYRO_ONLY)
		rslt = bmi2_set_fifo_wm(600, dev);//6*100=600
		if (rslt != BMI2_OK) 
		{
			PDEBUG("bmi2_set_fifo_wm error, error code: %d\r\n", rslt);
		}
		rslt = bmi2_set_fifo_config(BMI2_FIFO_GYR_EN , BMI2_ENABLE, dev);
		#elif defined(ACC_GYRO)
		rslt = bmi2_set_fifo_wm(1200, dev);//12*100=1200
		if (rslt != BMI2_OK) 
		{
			PDEBUG("bmi2_set_fifo_wm error, error code: %d\r\n", rslt);
		}
		rslt = bmi2_set_fifo_config(BMI2_FIFO_ACC_EN | BMI2_FIFO_GYR_EN , BMI2_ENABLE, dev);
		#endif
		if (rslt != BMI2_OK)
		{
			PDEBUG("Set fifo config failed\r\n");
		}
	#endif

	bmi2_set_fifo_config(BMI2_FIFO_HEADER_EN, BMI2_DISABLE, dev);
#endif

	/* Flush FIFO */
	rslt = bmi2_set_command_register(BMI2_FIFO_FLUSH_CMD, dev);
	if (rslt != BMI2_OK) 
	{
		PDEBUG("bmi2_set_command_register Error code: %d\r\n", rslt);
	}

	#if defined(FIFO_WM_INT)
	bmi2_get_int_pin_config(&int_cfg, dev);
	
	int_cfg.pin_type = BMI2_INT2;
	int_cfg.pin_cfg[1].lvl = BMI2_INT_ACTIVE_HIGH;
	int_cfg.pin_cfg[1].od = BMI2_INT_PUSH_PULL;;
	int_cfg.pin_cfg[1].output_en= BMI2_INT_OUTPUT_ENABLE;

	bmi2_set_int_pin_config(&int_cfg, dev);

	/* Map interrupt to pins*/
	rslt = bmi2_map_data_int(sens_int, BMI2_INT2, dev);
	if (rslt != BMI2_OK) 
	{
		PDEBUG("Map data INT1 failed, error code: %d\r\n", rslt);
	}
	#endif

	/* Update FIFO structure */
	fifoframe.data = fifo_data;

	#if defined(SUPPORT_LOWPOWER)
	/* Enable advanced power save */
	rslt = bmi2_set_adv_power_save(BMI2_ENABLE, dev);
	if (rslt != BMI2_OK) 
	{
		PDEBUG("bmi2_set_adv_power_save Error code: %d\r\n", rslt);
	}
	#endif
	
	return rslt;
}
#endif

int8_t Close_BMI270_FIFO(struct bmi2_dev *dev)
{
	int8_t rslt = BMI2_OK;
	
	/* Disable advanced power save */
	rslt = bmi2_set_adv_power_save(BMI2_DISABLE, dev);
	if (rslt != BMI2_OK) 
	{
		PDEBUG("Error code: %d\r\n", rslt);
		return rslt;
	}

	/* Flush FIFO */
	rslt = bmi2_set_command_register(BMI2_FIFO_FLUSH_CMD, dev);
	if (rslt != BMI2_OK) 
	{
			PDEBUG("Flush FIFO error, error code: %d\r\n", rslt);
			return rslt;
	}

	/* Clear FIFO configuration register */
	rslt = bmi2_set_fifo_config(BMI2_FIFO_ALL_EN, BMI2_DISABLE, dev);
	if (rslt != BMI2_OK) 
	{
		PDEBUG("Clear FIFO configuration register error, error code: %d\r\n", rslt);
		return rslt;
	}
	
	return rslt;
}

int8_t Modify_BMI270_STEP_COUNTER_Parameter(struct bmi2_dev *dev)
{
	int8_t rslt = BMI2_OK;
	uint8_t i;
	/* Sensor configuration structure */
	struct bmi2_sens_config config = { 0 };
	
	config.type = BMI2_STEP_COUNTER_PARAMS;
	/* Get the previous or default configuration settings */
	rslt = bmi270_get_sensor_config(&config, 1, dev);
	if (rslt == BMI2_OK) 
	{
		for(i = 0; i < BMI2_STEP_CNT_N_PARAMS; i++)
		{
			PDEBUG("Step counter offset: %d, value: %d\r\n", i+1, config.cfg.step_counter_params[i]);
		}
	}

	config.cfg.step_counter_params[4] = 10;
	rslt = bmi270_set_sensor_config(&config, 1, dev);
	if (rslt == BMI2_OK) 
	{
		PDEBUG("bmi2_set_sensor_config successful\r\n");
	}
	else
	{
		PDEBUG("bmi2_set_sensor_config failed\r\n");
	}

	rslt = bmi270_get_sensor_config(&config, 1, dev);
	if (rslt == BMI2_OK) 
	{
		for(i = 0; i < BMI2_STEP_CNT_N_PARAMS; i++)
		{
			PDEBUG("Step counter offset: %d, value: %d\r\n", i+1, config.cfg.step_counter_params[i]);
		}
	}
	
	return rslt;
}

int8_t Open_BMI270_STEP_COUNTER(struct bmi2_dev *dev)
{
	int8_t rslt = BMI2_OK;
	uint8_t sensor_list = BMI2_STEP_COUNTER;
	/* Sensor configuration structure */
	struct bmi2_sens_config config = { 0 };

	Modify_BMI270_STEP_COUNTER_Parameter(dev);

	/* Enable step counter */
	rslt = bmi270_sensor_enable(&sensor_list, 1, dev);
	bmi2_error_codes_print_result(rslt);

	config.type = sensor_list;

	/* Get the previous or default configuration settings */
	rslt = bmi270_get_sensor_config(&config, 1, dev);
	if (rslt == BMI2_OK) 
	{
		/* Set water-mark level with a resolution of 20 counts
		for step counter */
		//config[2].cfg.step_cnt.count_4.wm_lvl = 1;
		config.cfg.step_counter.watermark_level = 1;
		config.cfg.step_counter.step_buffer_size = 7;

		rslt = bmi270_set_sensor_config(&config, 1, dev);
		if (rslt != BMI2_OK) 
		{
			PDEBUG("enable step counter config error=%\r\n", rslt);
		}
	}

	return rslt;
}

int8_t Close_BMI270_STEP_COUNTER(struct bmi2_dev *dev)
{
	int8_t rslt = BMI2_OK;
	uint8_t sensor_list = BMI2_STEP_COUNTER;

	/* Enable step counter */
	rslt = bmi270_sensor_disable(&sensor_list, 1, dev);
	bmi2_error_codes_print_result(rslt);
	
	return rslt;
}

void BMI270_Print_ALLRegs(struct bmi2_dev *dev)
{
	uint8_t value;

	uint8_t reg_addr;

	for(reg_addr = 0; reg_addr < 0x7E; reg_addr++)
	{
		bmi2_get_regs(reg_addr, &value, 1, dev);
		PDEBUG("0x%02X, value=0x%02X\r\n", reg_addr, value);
	}
}

int8_t Init_BMI270(struct bmi2_dev *dev)
{
	int8_t rslt = BMI2_OK;
	uint8_t chipid;
	uint8_t ver_major, ver_minor;

	/* Interface reference is given as a parameter
	* For I2C : BMI2_I2C_INTF
	* For SPI : BMI2_SPI_INTF
	*/
	rslt = bmi2_interface_init(dev, BMI2_I2C_INTF);
	bmi2_error_codes_print_result(rslt);

	/* Initialize bmi270. */
	rslt = bmi270_init(dev);
	bmi2_error_codes_print_result(rslt);

	if (rslt != BMI2_OK)
	{
		PDEBUG("bmi270_wh_init() failed, error code: %d\r\n", rslt);
		return rslt;
	}
	else
	{
		rslt = bmi2_get_regs(BMI2_CHIP_ID_ADDR, &chipid, 1, dev);
		if (rslt != BMI2_OK) 
		{
			PDEBUG("read chip ID failed, error code: %d\r\n", rslt);
			return rslt;
		}

		PDEBUG("Chip ID:%02x\r\n", chipid);
	}

	PDEBUG("BMI270 initialized successfully\r\n");

	rslt = bmi2_get_config_file_version(&ver_major, &ver_minor, dev);
	PDEBUG("The firmware version: v%d.%d\r\n", ver_major, ver_minor);

	/*Axis mapping according phisical structure*/
#if 0//For smart watch axis remap
	struct bmi2_remap remapped_axis;

	rslt = bmi2_get_remap_axes(&remapped_axis, dev);
	if(rslt != BMI2_OK)
	{
		PDEBUG("bmi270 org_axis: x=%02X, y=%02X, z=%02X\r\n", remapped_axis.x, remapped_axis.y, remapped_axis.z);
	}

	remapped_axis.x = BMI2_X;
	remapped_axis.y = BMI2_Y;
	remapped_axis.z = BMI2_NEG_Z;

	rslt = bmi2_set_remap_axes(&remapped_axis, dev);
	if(rslt != BMI2_OK)
	{
		PDEBUG("bmi270 remapped axis failed\r\n");
	}
#endif
#if defined(ACC_SELFTEST)
	rslt = bmi2_perform_accel_self_test(dev);
	bmi2_error_codes_print_result(rslt);

	if(rslt == BMI2_OK)
	{
		PDEBUG("ACCEL SELF TEST RESULT SUCCESS\r\n");
	} 
	else 
	{
		PDEBUG("ACCEL SELF TEST RESULT FAIL\r\n");
		return rslt;
	}

#elif defined(GYRO_SELFTEST)
	uint8_t sensor_list = BMI2_GYRO;

	/* Enable the selected sensors */
	rslt = bmi2_sensor_enable(&sensor_list, 1, dev);
	bmi2_error_codes_print_result(rslt);
	rslt = bmi2_gyro_mems_self_test(dev);
	if(rslt == BMI2_OK)
	{
		PDEBUG("GYRO SELF TEST RESULT SUCCESS\r\n");
	} 
	else 
	{
		PDEBUG("GYRO SELF TEST RESULT FAIL\r\n");
		return rslt;
	}

#elif defined(ACC_GYRO_SELFTEST)
	uint8_t sensor_list = BMI2_GYRO;

	rslt = bmi2_perform_accel_self_test(dev);
	if(rslt == BMI2_OK)
	{
		PDEBUG("ACCEL SELF TEST RESULT SUCCESS\r\n");
	} 
	else 
	{
		PDEBUG("ACCEL SELF TEST RESULT FAIL\r\n");
		return rslt;
	}

	/* Software Reset */
	rslt = bmi2_set_command_register(BMI2_SOFT_RESET_CMD, dev);
	if (rslt != BMI2_OK) 
	{
		PDEBUG("bmi2_set_command_register software reset Error code: %d\r\n", rslt);
		return rslt;
	}

	rslt = bmi270_init(dev);
	if (rslt != BMI2_OK)
	{
		PDEBUG("bmi270_init() failed, error code: %d\r\n", rslt);
		return rslt;
	}

	/* Enable the selected sensors */
	rslt = bmi2_sensor_enable(&sensor_list, 1, dev);
	bmi2_error_codes_print_result(rslt);
	rslt = bmi2_gyro_mems_self_test(dev);
	if(rslt == BMI2_OK)
	{
		PDEBUG("GYRO SELF TEST RESULT SUCCESS\r\n");
	} 
	else 
	{
		PDEBUG("GYRO SELF TEST RESULT FAIL\r\n");
	}
	for(;;)
	{
		
	}	
#elif defined(CRT)
	uint8_t sens_sel[2] = { BMI2_ACCEL, BMI2_GYRO };

	rslt = bmi2_sensor_enable(&sens_sel[0], 1, dev);    
	bmi2_perform_accel_self_test(rslt);    
	
	rslt = bmi2_sensor_disable(&sens_sel[1], 1, dev);    
	bmi2_perform_accel_self_test(rslt);    
	
	sensor_data.type = BMI2_ACCEL;    
	dev.delay_us(100000);    
	PDEBUG("before CRT Accel x,y,z values\r\n");

	/* read the accel data before CRT*/    
	rslt = bmi2_get_sensor_data(&sensor_data, 1, dev);    
	bmi2_perform_accel_self_test(rslt);    
	PDEBUG("X axes: %d, Y axes: %d, Z axes: %d\r\n", sensor_data.sens_data.acc.x, sensor_data.sens_data.acc.y, sensor_data.sens_data.acc.z );    

	/*brief This API is to run the CRT process*/    
	rslt = bmi2_do_crt(dev);    
	bmi2_perform_accel_self_test(rslt);    
	if(rslt == BMI2_OK)    
	{    	
		PDEBUG("After CRT Accel x,y,z values\r\n");    	
		/* read the accel data after CRT*/    	
		rslt = bmi2_get_sensor_data(&sensor_data, 1, dev);    	
		bmi2_perform_accel_self_test(rslt);    	
		PDEBUG("X axes: %d, Y axes: %d, Z axes: %d\r\n",sensor_data.sens_data.acc.x, sensor_data.sens_data.acc.y, sensor_data.sens_data.acc.z );    
	}
#endif
	/* Disable advanced power save */
	rslt = bmi2_set_adv_power_save(BMI2_DISABLE, dev);
	bmi2_error_codes_print_result(rslt); 


	#if defined(ACC_ONLY)
	Open_BMI270_ACC(dev);
	Close_BMI270_GYRO(dev);
	#elif defined(GYRO_ONLY)
	Close_BMI270_ACC(dev);
	Open_BMI270_GYRO(dev);
	#elif defined(ACC_GYRO)
	Open_BMI270_ACC(dev);
	Open_BMI270_GYRO(dev);
	#endif

	#if defined(WRIST_WEAR_WAKE_UP)
	Open_BMI270_WRISTWEAR_WAKEUP(dev);
	#endif
	
	#if defined(STEP_COUNTER)
	Open_BMI270_STEP_COUNTER(dev);
	#endif

	#if defined(FIFO_POLL)
	Open_BMI270_FIFO(dev);
	#elif defined(FIFO_WM_INT)
	Open_BMI270_FIFO(dev);
	Enable_MCU_INT2_Pin();
	#endif

	#if defined(WRIST_WEAR_WAKE_UP)
	Enable_MCU_INT1_Pin();
	#endif

	#if !defined(FIFO_WM_INT)
	Disable_MCU_INT2_Pin();
    Disable_MCU_INT1_Pin();
	#endif

	#if defined(SUPPORT_LOWPOWER)
	/* Enable advanced power save */
	rslt = bmi2_set_adv_power_save(BMI2_ENABLE, dev);
	if (rslt != BMI2_OK) 
	{
		PDEBUG("bmi2_set_adv_power_save Error code: %d\r\n", rslt);
	}
	#endif
	
	return rslt;
}

//#if defined(FIFO_POLL) || defined(STEP_COUNTER)
//void BMI270_Timer_Callback(TimerHandle_t xTimer)
//{
//	fifo_read_ready = 1;
//}
//#endif
void Init_bmi270() {
    int8_t rslt = BMI2_OK;
    struct bmi2_dev *dev;
    dev = &bmi270dev;
    Init_BMI270(dev);
}
void StartBMI270Task()
{
	int8_t rslt = BMI2_OK;
	struct bmi2_dev *dev;
	dev = &bmi270dev;

//#if defined(WRIST_WEAR_WAKE_UP)
//	osThreadId BMI270InterruptTaskHandle;
//	osThreadDef(BMI270INTERRUPTTask, StartBMI270InterruptTask, osPriorityNormal, 0, 256);
//  	BMI270InterruptTaskHandle = osThreadCreate(osThread(BMI270INTERRUPTTask), NULL);
//#endif

	Init_BMI270(dev);
	
//	#if defined(FIFO_POLL) || defined(STEP_COUNTER)
//	BMI270_Timer_Handler = xTimerCreate("FIFOReadTimer", 1010, pdTRUE, (void *)1, BMI270_Timer_Callback);
//	if(BMI270_Timer_Handler != NULL)
//	{
//		xTimerStart(BMI270_Timer_Handler, 1000);
//	}
//	#endif

	for(;;)
	{
		if(fifo_read_ready == 1)
		{
			#if defined(FIFO_POLL) || defined(FIFO_WM_INT)
			/* Disable advanced power save */
			rslt = bmi2_set_adv_power_save(BMI2_DISABLE, dev);
			if (rslt != BMI2_OK)
			{
				PDEBUG("bmi2_set_adv_power_save Error code: %d\r\n", rslt);
			}

			memset(fifo_data, 0, sizeof(fifo_data));
			fifoframe.length = 0;
			accel_length = 0;
			gyr_length = 0;

			/* Update FIFO structure */
			fifoframe.data = fifo_data;

			/* Get fifo length */
			rslt = bmi2_get_fifo_length(&(fifoframe.length), dev);
			if (rslt != BMI2_OK)
			{
				PDEBUG("bmi2_get_fifo_length, error code: %d\r\n", rslt);
			}
			//PDEBUG("Fifo length=%d\r\n", fifoframe.length);
			if(fifoframe.length > 0)
			{
				accel_length = fifoframe.length;
				gyr_length = fifoframe.length;

				/* Read FIFO data */
				rslt = bmi2_read_fifo_data(&fifoframe, dev);
				#if defined(SUPPORT_LOWPOWER)
				/* Enable advanced power save */
				bmi2_set_adv_power_save(BMI2_ENABLE, dev);
				#endif
				PDEBUG("bmi2_read_fifo_data, rslt=%d\r\n", rslt);
				bmi2_error_codes_print_result(rslt);
				if (rslt == BMI2_OK)
				{
					PDEBUG("Read %d bytes from fifo\r\n", fifoframe.length);

					#if defined(ACC_ONLY) || defined(ACC_GYRO)

					/* Parse the FIFO data to extract accelerometer data from the FIFO buffer */
					rslt = bmi2_extract_accel(fifo_accel_data, &accel_length, &fifoframe, dev);
					//PDEBUG("bmi2_extract_accel rslt=%d\r\n", rslt);
					if (rslt == BMI2_OK)
					{
						PDEBUG("Parsed accelerometer data frames: %d\r\n", accel_length);

						/* Print the parsed accelerometer data from the FIFO buffer */
						for(idx = 0 ; idx < accel_length ; idx++)
						{
							//PDEBUG("ACCEL[%d] X : %d , Y : %d , Z : %d\r\n", idx , fifo_accel_data[idx].x, fifo_accel_data[idx].y, fifo_accel_data[idx].z);
						}
					}
					#endif
					#if defined(GYRO_ONLY) || defined(ACC_GYRO)

					/* Parse the FIFO data to extract gyroscope data from the FIFO buffer */
					rslt = bmi2_extract_gyro(fifo_gyr_data, &gyr_length, &fifoframe, dev);
					//PDEBUG("bmi2_extract_gyro rslt=%d\r\n", rslt);
					if (rslt == BMI2_OK)
					{
						PDEBUG("Parsed gyroscope data frames: %d\r\n", gyr_length);

						/* Print the parsed accelerometer data from the FIFO buffer */
						for(idx = 0 ; idx < gyr_length ; idx++)
						{
							//PDEBUG("GYRO[%d] X : %d , Y : %d , Z : %d\r\n", idx , fifo_gyr_data[idx].x, fifo_gyr_data[idx].y, fifo_gyr_data[idx].z);
						}
					}
					#endif
				}
			}
			#endif
			#if defined(STEP_COUNTER)
			sensor_data_stepcounter.type = BMI2_STEP_COUNTER;
			rslt = bmi270_get_sensor_data(&sensor_data_stepcounter, 1, dev);
			if (rslt == BMI2_OK)
			{
				PDEBUG("Step count = %u\r\n", sensor_data_stepcounter.sens_data.step_counter_output);
			}
			#endif
			fifo_read_ready = 0;

		}
	}
}
void read_Gryo(uint16_t *res) {
    uint8_t raw[6];
    HAL_I2C_Mem_Read(&hi2c1, BMI2_I2C_PRIM_ADDR, BMI2_GYR_X_LSB_ADDR, 1, raw, 6, 100);
    res[0] = (raw[1] << 8) | raw[0];
    res[1] = (raw[3] << 8) | raw[2];
    res[2] = (raw[5] << 8) | raw[4];
}
void read_Accel(uint16_t *res) {
    uint8_t raw[6];
    HAL_I2C_Mem_Read(&hi2c1, BMI2_I2C_PRIM_ADDR, BMI2_ACC_X_LSB_ADDR, 1, raw, 6, 100);
    res[0] = (raw[1] << 8) | raw[0];
    res[1] = (raw[3] << 8) | raw[2];
    res[2] = (raw[5] << 8) | raw[4];
}

#if defined(WRIST_WEAR_WAKE_UP)
void StartBMI270InterruptTask(void const * argument)
{
	int8_t rslt = BMI2_OK;
	struct bmi2_dev *dev;

	dev = &bmi270dev;
	
	/* Variable to get wrist gesture status */
	uint16_t int_status = 0;
	
	for(;;)
	{
		if(int1_flag == 1)
        	{
			/* Check the interrupt status of the wrist gesture */
			rslt = bmi2_get_int_status(&int_status, dev);
			PDEBUG("bmi2_get_int_status\r\n");
			bmi2_error_codes_print_result(rslt);
			if (rslt == BMI2_OK)
			{
				PDEBUG("Get int status=0x%04X\r\n", int_status);
				if (int_status & BMI270_WRIST_WAKE_UP_STATUS_MASK)
				{
					PDEBUG("Wrist wear wakeup detected\r\n");
					
				}
			}
			int1_flag = 0;
		}
	}
}
#endif

