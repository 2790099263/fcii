/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "i2c.h"
#include "usb_device.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "bmi270_task.h"
#include "common_porting.h"
#include "bmi270.h"
#include "math.h"
#include "hmc5883l.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
extern struct bmi2_dev bmi270dev;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/*! Earth's gravity in m/s^2 */
#define GRAVITY_EARTH  (9.80665f)
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
static float lsb_to_mps2(int16_t val, float g_range, uint8_t bit_width)
{
    double power = 2;

    float half_scale = (float)((pow((double)power, (double)bit_width) / 2.0f));

    return (GRAVITY_EARTH * val * g_range) / half_scale;
}
static float lsb_to_dps(int16_t val, float dps, uint8_t bit_width)
{
    double power = 2;

    float half_scale = (float)((pow((double)power, (double)bit_width) / 2.0f));

    return (dps / (half_scale)) * (val);
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_USB_Device_Init();
  /* USER CODE BEGIN 2 */
  HAL_Delay(3000);
  for(int i=0;i<127;i++){
        uint8_t data;
        HAL_I2C_Mem_Read(&hi2c1, i<<1, 0x00, 1, &data, 1, 1000);
        if(data != 0x00){
            PDEBUG("I2C Addr: 0x%02X : ID: 0x%02X\n", i,data);
        }
  }
  PDEBUG("test\n");
//  StartBMI270Task();
    Init_bmi270();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  struct bmi2_sensor_data sens_data={0};
    int8_t rslt = BMI2_OK;
    struct bmi2_dev *dev;
    dev = &bmi270dev;
    float x = 0, y = 0, z = 0, ang = 0;
    int16_t QMC_X,QMC_Y,QMC_Z;
    QMC5883L_Initialize(MODE_CONTROL_CONTINUOUS,OUTPUT_DATA_RATE_100HZ,FULL_SCALE_2G,OVER_SAMPLE_RATIO_512);
  while (1)
  {
      HAL_GPIO_WritePin(GPIOC,GPIO_PIN_13,GPIO_PIN_SET);
      HAL_Delay(100);
      HAL_GPIO_WritePin(GPIOC,GPIO_PIN_13,GPIO_PIN_RESET);
      HAL_Delay(100);
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
    sens_data.type = BMI2_ACCEL;
    rslt = bmi2_get_sensor_data(&sens_data, 1,dev);
    bmi2_error_codes_print_result(rslt);
//      if (rslt == BMI2_OK)
//      {
          /* Converting lsb to meter per second squared for 16 bit accelerometer at 2G range. */
          x = lsb_to_mps2(sens_data.sens_data.acc.x, (float)2, dev->resolution);
          y = lsb_to_mps2(sens_data.sens_data.acc.y, (float)2, dev->resolution);
          z = lsb_to_mps2(sens_data.sens_data.acc.z, (float)2, dev->resolution);
          PDEBUG("Accel: %d, %d, %d, %f, %f, %f  %d\n",sens_data.sens_data.acc.x,sens_data.sens_data.acc.y,sens_data.sens_data.acc.z, x, y, z,rslt);
//      }
    sens_data.type = BMI2_GYRO;
    rslt = bmi2_get_sensor_data(&sens_data, 1,dev);
    bmi2_error_codes_print_result(rslt);
    x = lsb_to_dps(sens_data.sens_data.gyr.x, (float)2000, dev->resolution);
    y = lsb_to_dps(sens_data.sens_data.gyr.y, (float)2000, dev->resolution);
    z = lsb_to_dps(sens_data.sens_data.gyr.z, (float)2000, dev->resolution);
    PDEBUG("Gyro: %d, %d, %d, %f, %f, %f  %d\n",sens_data.sens_data.gyr.x,sens_data.sens_data.gyr.y,sens_data.sens_data.gyr.z, x, y, z,rslt);
    QMC5883L_Read_Data(&QMC_X,&QMC_Y,&QMC_Z);
    ang = QMC5883L_Heading(QMC_X,QMC_Y,QMC_Z);
    PDEBUG("Angle: %f\n\n",ang);
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV1;
  RCC_OscInitStruct.PLL.PLLN = 24;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV4;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
